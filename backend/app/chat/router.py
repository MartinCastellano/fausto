from fastapi import APIRouter
from .controller.controller import generate_chat, config_chat, clear_chat
from app.chat.model.model import GenerateModel, ConfigModel

router = APIRouter()


@router.post("/generate_chat")
def generate_in_chat(message: GenerateModel):
    return generate_chat(message)


@router.post("/config")
def config_in_chat(request: ConfigModel):
    return config_chat(request)


@router.post("/clear")
def clear_in_chat():
    clear_chat()
