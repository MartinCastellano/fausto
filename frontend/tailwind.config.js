/** @type {import('tailwindcss').Config} */
export default {
  mode: "jit",
  content: [
    // "./main.jsx",
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    // "./components/**/*.{js,ts,jsx,tsx}"
  ],
  darkMode: "media",
  theme: {
    extend: {},
  },
  plugins: [],
}

