from pydantic import BaseModel


class ConfigModel(BaseModel):
    prompt: str = ""
    temperature: float = 0.9
    max_length: int = 512
    model_name: str = "stabilityai/StableBeluga2"
    top_p: float = 0.6
    top_k: int = 90
    max_new_tokens: int = 1
    custom_model_check :bool = False
    custom_model :str = ''
    hugging_face_token : str = ''
    hugging_face_token_check : bool = False

class GenerateModel(BaseModel):
    prompt: str
