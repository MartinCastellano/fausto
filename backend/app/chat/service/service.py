from app.chat.model.model import ConfigModel
from transformers import AutoTokenizer,LlamaTokenizer
# from huggingface_hub
from petals import AutoDistributedModelForCausalLM
# from ctransformers import AutoModelForCausalLM


class ConfigService:
    def __init__(self):
        self.model = ConfigModel()


class GenerateService:
    def __init__(self, config:ConfigModel=None):        
        self.config_model = ConfigService()        
        if config!=None:            
            self.config(config)            
        else:
            self.modelLLM = AutoDistributedModelForCausalLM.from_pretrained(self.config_model.model.model_name)
            # self.modelLLM = self.modelLLM.cuda()
            self.tokenizer = AutoTokenizer.from_pretrained(self.config_model.model.model_name, use_fast=False, add_bos_token=False )

    def config(self, config: ConfigModel):        
        print(config)
        self.config_model.model.prompt = config.prompt
        self.config_model.model.temperature = config.temperature
        self.config_model.model.max_length = config.max_length        
        self.config_model.model.top_p = config.top_p
        self.config_model.model.top_k = config.top_k
        self.config_model.model.max_new_tokens = config.max_new_tokens
        if self.config_model.model.custom_model_check:
            if self.config_model.model.hugging_face_token_check:
                #ver como es el tema si necesita esto
                self.config_model.model.hugging_face_token = config.hugging_face_token   
            self.config_model.model.model_name = config.custom_model
        else:
            self.config_model.model.model_name = config.model_name     
        self.LLMConfig()
        
    def LLMConfig(self):
        self.modelLLM = AutoDistributedModelForCausalLM.from_pretrained(self.config_model.model.model_name)
        # self.modelLLM = self.modelLLM.cuda()
        self.tokenizer = AutoTokenizer.from_pretrained(self.config_model.model.model_name, use_fast=False, add_bos_token=False )    
        
        
    def generate(self, prompt):
      
      fake_token = self.tokenizer("^")["input_ids"][0]
      with self.modelLLM.inference_session(max_length=self.config_model.model.max_length) as sess:
          prefix = f"Human: {prompt}\nFriendly AI:"
          prefix = self.tokenizer(prefix, return_tensors="pt")["input_ids"]
          generated_text = ''
          while True:
            outputs = self.modelLLM.generate(prefix, max_new_tokens=self.config_model.model.max_new_tokens, session=sess,
                                            do_sample=True, temperature=self.config_model.model.temperature, top_p=self.config_model.model.top_p)
            outputs = self.tokenizer.decode([fake_token, outputs[0, -1].item()])[1:]            
            generated_text += outputs
            if "\n" in outputs:
                break
            prefix = None
            
          
          return f"\nHuman: {prompt}\n{self.config_model.model.model_name}: {generated_text}\n"  
        
      
    
    def clear_chat(self):
        self.config_model.model.prompt = ""
