model_name = "stabilityai/StableBeluga2"
# from ctransformers import AutoModelForCausalLM
# llm = AutoModelForCausalLM.from_pretrained('/path/to/ggml-gpt-2.bin', model_type='gpt2')

from transformers import AutoTokenizer
from petals import AutoDistributedModelForCausalLM

# model_name = "enoch/llama-65b-hf"
# You can also use "bigscience/bloom" or "bigscience/bloomz"
tokenizer = AutoTokenizer.from_pretrained(
    model_name, use_fast=False, add_bos_token=False
)

model = AutoDistributedModelForCausalLM.from_pretrained(model_name)
# Embeddings & prompts are on your device, transformer blocks are distributed across the Internet
model = model.cuda()
# tokens = tokenizer.encode('AI is going to')
# for token in llm.generate(tokens):
#     print(tokenizer.decode(token))
inputs = tokenizer('A cat in French is "', return_tensors="pt")["input_ids"].cuda()
# inputs = tokenizer("A cat sat")["input_ids"]
outputs = model.generate(inputs, max_new_tokens=5).cuda()
print(tokenizer.decode(outputs[0]))
