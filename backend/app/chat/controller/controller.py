from app.chat.model.model import GenerateModel, ConfigModel
import app.chat.service.service as service
from fastapi import HTTPException
from app.exceptions import exceptions

     
generate_chat_service = service.GenerateService()


def generate_chat(prompt_message: GenerateModel):
    try:
        return generate_chat_service.generate(prompt_message.prompt)

    except exceptions.NotFoundException as e:
        raise HTTPException(
            status_code=e.status_code,
            detail=e.message,
        )


def config_chat(config: ConfigModel):
    try:
        generate_chat_service = service.GenerateService(config)

    except exceptions.NotFoundException as e:
        raise HTTPException(
            status_code=e.status_code,
            detail=e.message,
        )


def clear_chat():
    try:
        generate_chat_service.clear_chat()

    except exceptions.NotFoundException as e:
        raise HTTPException(
            status_code=e.status_code,
            detail=e.message,
            )
# if __name__ == "__main__":         
