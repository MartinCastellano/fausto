from fastapi import FastAPI
from app.chat import chat_router

# import uvicorn
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
import os

app = FastAPI()
app.include_router(chat_router, prefix="/api")

app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:31415", "http://192.168.0.191:31415"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def index():
    return {"title": "Hello World :)"}




if os.path.exists('fausto'):     
    app.mount('/static', StaticFiles(directory='fausto/backend/dist', html=True),name="/static")
elif os.path.exists('backend'):    
    app.mount('/static', StaticFiles(directory='backend/dist', html=True),name="/static")
else:    
    app.mount('/static', StaticFiles(directory='dist', html=True),name="/static")  

# if __name__ == "__main__":
#     uvicorn.run("app.api:app", host="0.0.0.0", port=8000, reload=True)
