import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'


// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],  
  server: { 
    proxy:{
      "/api" : "http://localhost:8000" ,
    },   
    host: true,    
    port: 31415,
  },
  hmr: { clientPort: 31415 },
  base: '',
  root: '',  
  build: {
    outDir: 'dist',
    public:  '../public',
    
    

  },
  
 
})

