import { useState } from 'react';
import axios from 'axios';


export function Form(){
    
    const options = [
        { value: 'stabilityai/StableBeluga2', label: 'Beluga2' }, 
        { value: 'codellama/CodeLlama-34b-Instruct-hf', label: 'CodeLlama-34b' },              
        { value: 'meta-llama/Llama-2-70b-chat-hf', label: 'Llama2-70b' },
        { value: 'huggyllama/llama-65b', label: 'Llama-65b' },
        { value: 'bigscience/bloomz', label: 'BloomZ' },
        { value: 'bigscience/bloom', label: 'Bloom' }
      ]
    const [showCustomModel, setShowCustomModel] = useState(false);    
    const [showcustomModelToken, setShowcustomModelToken] = useState(false);    
    const [customModelValue, setCustomModelValue] = useState('');
    const [customModelToken, setcustomModelToken] = useState('');
    const [selected, setSelected] = useState(options[0].value);  
    const [inputText, setInputText] = useState('');
    
    const [chatBoxes, setChatBoxes] = useState([0]);
    

    let [chatJson, setChatJson] = useState({
        prompt: "humanso lalasl", //a eliminar ?
        temperature: 0.5,
        model_name: "nombre modelo",   
        max_length: 512,
        top_p: 0.6,
        top_k: 90,
        max_new_tokens: 1,
        custom_model_check : false,
        custom_model : '',
        hugging_face_token : '',
        hugging_face_token_check : false
    });

    const handleSliderChange = (sliderName, event) => {
        const newValue = parseFloat(event.target.value);
        setChatJson((prevChatJson) => ({
          ...prevChatJson,
          [sliderName]: newValue,
        }));
    };

    const handleConfig = async () => {
        try{
            let updatedChatJson = {
                ...chatJson,                
                hugging_face_token: customModelToken,

            };
            console.log(updatedChatJson)
            const response = await axios.post('/api/config',updatedChatJson);
            console.log("Response from backend:", response.data);
                
        } catch (error) {
            console.error("Error:", error);
        }
        

    };


    const handleGenerate = async () => {
        try {
            // timeout: 5000,
            // headers: {
            // Accept: 'application/json',
            // },
            let inputText = await document.querySelector('.input-text').value;
            if (inputText !== "") {                  
                chatJson.prompt += inputText;
                let prompt = chatJson.prompt;
                              
                const response = await axios.post('/api/generate_chat', {
                    prompt,
                });

                chatJson.prompt = response.data 
                // response.setHeader('Connection', 'close')
                // response.end('Request handled :)')   
                document.querySelector('.input-text').value = '';
                setInputText("");
            }
            
         } catch (error) {
            console.log(error);
         }
        
      };
    

    const handleClear = async () => {
        await axios.post('/api/clear');
        setChatJson((prevChatJson) => ({
            ...prevChatJson,
            ['prompt']: '',
          }));
    };

    
    const handleSelectChange = event => {
        
        setSelected(event.target.value);
        setChatJson((prevChatJson) => ({
            ...prevChatJson,
            model_name : event.target.value,
          }));
    };
    const handleNewChat = () => {
        
        
        const newChatBox = {
            id: Date.now(), 
            content: []
          };
        setChatBoxes(prevChatBoxes => [...prevChatBoxes, newChatBox]);
       
        setChatJson(prevChatJson => ({
            ...prevChatJson,
            prompt: '',
        }));
    };
    const handleChatClear = () => {
        if (chatBoxes.length > 0) {
          
          const updatedChatBoxes = [...chatBoxes];
          
          updatedChatBoxes.pop();
          
          setChatBoxes(updatedChatBoxes);
        }
      };
    

    const handleCustomModelCheckChange = () => {
        setChatJson((prevChatJson) => ({
            ...prevChatJson,
            custom_model_check: !prevChatJson.custom_model_check,
        }));
    }; 
    const handleHuggingFaceTokenCheckChange = () => {
        setChatJson((prevChatJson) => ({
            ...prevChatJson,
            hugging_face_token_check: !prevChatJson.hugging_face_token_check,
        }));
    };
    
    const handleCustomModelValueChange = (newValue) => {
        setCustomModelValue(newValue);
        setChatJson((prevChatJson) => ({
            ...prevChatJson,
            custom_model: newValue,
        }));
    };
    
    return (
        
        <div className="form-container  w-full h-screen flex border rounded-lg border-black mt-auto ">
            <div className="left-column w-1/4 p-4  bg-gray-800 border-4 border-white rounded-lg  flex flex-col justify-between   ">
                <div className="border border-black bg-white rounded-lg h-10 ">
                   <button className=" bg-white rounded-lg  mb-2" onClick={handleNewChat}>New chat</button>
                    
                    <div className= "overflow-y-auto  max-h-[850px]   ">
                    {chatBoxes.map(chatBox => (
                        <div key={chatBox.id} className="bg-white border  border-black m-4 p-4 rounded-lg">
                        {chatJson.prompt}
                        </div>
                    ))} 
                    </div>
                 
                </div>
                
                <button className="border border-black  bg-white rounded-lg mb-2 h-10" onClick={handleChatClear}>Clear</button>
            </div>

            <br />             
            <div className="center-column  flex flex-col justify-between w-1/2 p-4 bg-gray-800 border-4 rounded-lg border-white ">
                <p className="text-left text-white">output:</p>
                <div className="output-box whitespace-normal break-words  rounded-lg flex-1 border bg-gray-400 border-black p-2 overflow-x-auto text-left">
                {/* {chatJson.prompt} */}
                {chatJson.prompt.split('\n').map((line, index) => (
                    <p key={index}>{line}</p>
                ))} 
                </div>
                <br/>
                <p className="text-left text-white" >input:</p>
                <textarea 
                className="input-text border   bg-gray-400 border-black rounded-lg  text-black placeholder-gray-200"
                type="text"                
                placeholder=" text prompt..."
                value={inputText}
                onChange={(e) => setInputText(e.target.value)}
                />
                
                <br></br>
                <div className="flex ">
                    <button className="border border-black p-2  rounded-lg mb-2  bg-white   w-full"  onClick={handleClear}>Clear</button>
                    <button className="border border-black p-2  rounded-lg mb-2   bg-white   w-full"  onClick={handleGenerate}>Generate</button>
                </div>
                
            </div>
            <div className='right-column w-2/6 p-4 bg-gray-800 border-4 rounded-lg  border-white '>
                <p className="text-left mb-8 text-white" >options:</p>
                
                <div className="flex">
                <div></div>
                <div></div>
                <div></div>    
                <ul className="flex-1 text-left  space-y-20 mb-8  text-white ">
                    
                    <li className="">
                        <span >temperature:</span>                        
                        <input  
                        type="range"   
                        min="0"  
                        max="1" 
                        step="0.01" 
                        value={chatJson.temperature}
                        onChange={(e) => handleSliderChange('temperature', e)}                         
                        />
                        <span >{chatJson.temperature}</span>                        
                    </li>
                    <li className="">
                        <span >top_p:</span>                                            
                        <input  
                        type="range"   
                        min="0"  
                        max="1" 
                        step="0.1" 
                        value={chatJson.top_p}
                        onChange={(e) => handleSliderChange('top_p', e)}
                        />
                        <span  >{chatJson.top_p}</span>
                    </li>
                    <li className="">
                        <span >top_k:</span>                                           
                        <input  
                        type="range"   
                        min="0"  
                        max="100" 
                        step="1" 
                        value={chatJson.top_k}
                        onChange={(e) => handleSliderChange('top_k', e)}
                        />
                        <span >{chatJson.top_k}</span>
                        
                    </li>
                    <li className="">
                        <span >max_length:</span>                    
                        <input  
                        type="range"   
                        min="0"  
                        max="1024" 
                        step="1" 
                        value={chatJson.max_length}
                        onChange={(e) => handleSliderChange('max_length', e)}
                        />
                        <span >{chatJson.max_length}</span>
                    </li>
                    <li className="flex items-center space-x-2 ">
                        <span >max_new_tokens:</span>                    
                        <input  
                        type="range"   
                        min="0"  
                        max="100"
                        step="1" 
                        value={chatJson.max_new_tokens}
                        onChange={(e) => handleSliderChange('max_new_tokens', e)}
                        />
                        <span >{chatJson.max_new_tokens}</span>
                    </li>
                    
                </ul>
                
                </div>
                <div>
                <div className="flex items-left mb-4 text-white">
                    <input
                        type="checkbox"
                        checked={showCustomModel}
                        onChange={() =>{
                            setShowCustomModel(!showCustomModel);
                            handleCustomModelCheckChange();
                            
                        }}                        
                        className="mr-2"
                    />
                    <label htmlFor="toggleTextBox">Custom model</label>
                </div>

                {showCustomModel && (
                    <>
                        <div className="flex items-left ">                            
                        </div>

                        {(
                            <input
                                type="text"
                                value={customModelValue}
                                onChange=
                                {(e) => {
                                    setCustomModelValue(e.target.value);
                                    handleCustomModelValueChange(e.target.value);
                                    
                                }}
                                className="rounded-lg border border-black bg-gray-400 text-black p-2 mb-2"
                            />
                        )}

                        { (
                            <div className="flex items-left mb-2 text-white">
                                <input
                                    type="checkbox"
                                    checked={showcustomModelToken}
                                    onChange={() =>{
                                        setShowcustomModelToken(!showcustomModelToken)
                                        handleHuggingFaceTokenCheckChange();
                                        // if (!showcustomModelToken) {
                                        //     setcustomModelToken('');
                                        // }
                                    }} 
                                    className="mr-2"
                                />
                                <label htmlFor="togglecustomModelToken">hugging face token</label>
                            </div>
                        )}

                        { showcustomModelToken && (
                            <input
                                type="password"
                                value={customModelToken}
                                onChange={(e) => {
                                    setcustomModelToken(e.target.value);                                    
                                }}
                                
                                className="rounded-lg border border-black bg-gray-400 text-black p-2 mb-2"
                            />
                        )}
                    </>
                )}

                {!showCustomModel && (
                    <select
                        className="rounded-lg border border-black flex h-10 mb-2"
                        value={selected}
                        onChange={handleSelectChange}
                    >
                        {options.map(option => (
                            <option key={option.value} value={option.value}>
                                {option.label}
                            </option>
                        ))}
                    </select>
                )}
                <button className="w-full flex items-center justify-center border border-black p-2 rounded-lg mb-2 bg-white" onClick={handleConfig}>Config</button>

                </div>
   
            </div>
        </div>
    );
        
}